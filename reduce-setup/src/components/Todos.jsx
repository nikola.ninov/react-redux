import { useDispatch, useSelector } from "react-redux";
import { removeTodo } from '../features/todo/todoSlice';

const Todos = () => {
    const todos = useSelector(state => state.todoSlice.todos);
    const dispatch = useDispatch();
    const deleteHandler = (id) => {
        dispatch(removeTodo(id));
    }

    return (
        <div>
            <h1>Todos</h1>

            <ul className="list-none">
                {todos && todos.map((todo) => (
                    <li key={todo.id} className="mt-4 flex justify-between items-center bg-zinc-800 px-4 py-2 rounded">
                        <div className="text-white text-center w-full">{todo.text}</div>
                        <button onClick={() => deleteHandler(todo.id)} className="text-white bg-red-500 border-0 py-1 px-4 focus:outline none hover:bg-red-600 rounded text-md">Delete</button>
                    </li>
                ))}
            </ul>

        </div>
    )
}

export default Todos;