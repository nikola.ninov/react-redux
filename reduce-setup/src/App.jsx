import './App.css'
import AddTodo from './components/AddTodo'
import Todos from './components/Todos'

function App() {
  return (
    <div>

      <h1 className="bg-red-500 text-white">
        Todo
      </h1>
      <AddTodo />
      <Todos />
    </div>
  )
}

export default App
